# code_samples

## Node js samples

- Middleware to upload file which save the file to google cloud bucked and strore file url to the database
- 

``` "use strict";
//CHANGE THIS IF SWITCH TO AWS OR DIGITAL OCEON
const { Storage } = require("@google-cloud/storage");
const fs = require("fs");
const crypto = require("crypto");

const gcs = new Storage({
  projectId: "xxxx",
  keyFilename: "./config/xxxxx.json"
});

let bucketName = "";

if(process.env.NODE_ENV === "production"){
  bucketName = "lastcampus";
}
else {
  bucketName = "lastcampus-development"
}
const bucket = gcs.bucket(bucketName);

function getPublicUrl(filename) {
  return "https://storage.googleapis.com/" + bucketName + "/" + filename;
}

let FileUpload = {};

FileUpload.uploadToGcs = route => (req, res, next) => {
  if (!req.file) return next();

  // Can optionally add a path to the gcsname below by concatenating it before the filename
  const gcsname = crypto.randomBytes(18).toString("hex") + "-" + req.file.originalname;
  let filename_with_path = gcsname;
  if (route) {
    filename_with_path = route + "/" + gcsname;
  }
  const file = bucket.file(filename_with_path);
  const stream = file.createWriteStream({
    metadata: {
      contentType: req.file.mimetype
    }
  });

  stream.on("error", err => {
    req.file.cloudStorageError = err;
    next(err);
  });

  stream.on("finish", () => {
    req.file.cloudStorageObject = gcsname;
    req.file.cloudStoragePublicUrl = getPublicUrl(filename_with_path);
    next();
  });

  stream.end(req.file.buffer);
};

module.exports = FileUpload;
```


## Route to register a new user to the lastcampus

```
// @route   POST request api/users/register
// @desc    Register user route
// @access  Public
router.post("/register", (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  //Check Validation
  const email = req.body.email;
  const emailRegex = new RegExp(email);

  User.findOne({ email: { $regex: emailRegex, $options: "i" } }).then(user => {
    if (user) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        institution_name: req.body.institution_name,
        password: req.body.password
      });

      if (req.body.is_teacher) {
        newUser.teacher.is_teacher = true;
        newUser.teacher.is_teacher_Verified = false;
        newUser.teacher.is_details_added = false;
      }

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(userRef => {
              console.log(userRef);
              Profile.findOne({ user: userRef._id })
                .then(profile => {
                  if (profile) {
                  } else {
                    // Create
                    let temporary_handle = req.body.name.split(" ")[0] + ":" + crypto.randomBytes(3).toString("hex");
                    const profile_fields = new Profile({
                      user: userRef._id,
                      handle: temporary_handle
                    });

                    if (req.body.is_teacher) {
                      profile_fields.teacher.verification_proof = req.body.verification_proof;
                      profile_fields.teacher.verification_picture = req.body.verification_picture;
                    }

                    // Check if Handle Exists
                    profile_fields
                      .save()
                      .then(profile => {
                        res.json(profile);
                      })
                      .catch(err => res.status(404).json(err));
                  }
                })
                .catch(err => res.json({ message: "Error" }));
            })
            .catch(err => console.log(err));
        });
      });
    }
  });
  //END
});

```



## Route to get notes of a student belong to a particular class of an institution

```
// @route   GET request api/notes/class/:page_number
// @desc    Get list of all class notes
// @access  Private
router.get("/class/:page_number", passport.authenticate("jwt", { session: false }), (req, res) => {
  Profile.findOne({ user: req.user.id }).then(profile => {
    Notes.find({
      "associated_with.institution_name": req.user.institution_name,
      "associated_with.faculty_name": profile.faculty_name,
      "associated_with.department_name": { $in: [profile.department_name, ""] },
      "associated_with.passing_year": { $in: [profile.passing_year, ""] }
    })
      .sort({ date: -1 })
      .skip((req.params.page_number - 1) * 10)
      .limit(10)
      .populate("uploaded_by.profile_id", ["handle", "profile_picture"])
      .populate("uploaded_by.user_id", ["name", "institution_name", "teacher"])
      .lean()
      .then(notes => {
        notes.map(note => {
          note.is_saved = note.saved.filter(save => save.user.toString() === req.user.id).length > 0 ? true : false;
          note.saved_count = note.saved.length;
          note.saved = [];
        });
        res.json(notes);
      })
      .catch(err => res.status(404).json({ message: "Error while retrieving notes" }));
  });
});
```


### Login page component of lastcampus app with react and redux

```
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { throws } from "assert";
import propTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser, loginUserFacebook } from "../../actions/authActions";
import TextFieldGroup from "../common/TextFieldGroup";
import Navbar from "../../component/Navbar";
import Paper from "@material-ui/core/Paper";
import classnames from "classnames";
import { LOGIN_BACK } from "../common/default_images";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardMedia from "@material-ui/core/CardMedia";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import SocialButton from "./SocialButton";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {}
    };
  }

  notifyRegister = () => {
    toast.info("Succesfully Registered! 😀", {
      position: toast.POSITION.BOTTOM_CENTER,
      autoClose: 15000
    });
  };

  notifySocialLogin = () => {
    toast.info("Please wait logging you in through Facebook", {
      position: "bottom-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true
    });
  };

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/profile");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/profile");
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    if (nextProps.location.state) {
      if (nextProps.location.state.is_Registered) {
        this.notifyRegister();
      }
    }
  }

  onChange = e =>
    this.setState({
      [e.target.name]: e.target.value
    });

  onSubmit = e => {
    e.preventDefault();
    const UserData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(UserData);
  };

  handleSocialLogin = user => {
    const loginData = {
      accessToken: user._token.accessToken
    };
    console.log(user);
    this.props.loginUserFacebook(loginData);
    this.notifySocialLogin();
  };

  handleSocialLoginFailure = err => {
    console.error(err);
    window.location.reload();
  };

  render() {
    const styles = {
      root: {
        flexGrow: 1
      },
      bullet: {
        display: "inline-block",
        margin: "0 2px",
        transform: "scale(0.8)"
      },
      title: {
        paddingTop: 10,
        fontSize: 22,
        color: "#000000"
      },
      pos: {
        marginBottom: 12
      },
      textField: {
        marginLeft: 10,
        marginRight: 10,
        width: "95%"
      },
      button: {
        margin: 4,
        backgroundColor: "#ffffff"
      },
      buttonLogin: {
        margin: 4,
        backgroundColor: "#0078BB"
      },
      paper: {
        textAlign: "center",
        marginTop: 130,
        overflow: "hidden",
        padding: 20
      }
    };
    const { errors } = this.state;
    return (
      <div>
        <ToastContainer />
        <Grid container spacing={0}>
          <Grid item lg={4} sm={12} xs={12} />
          <Grid item lg={4} sm={12} xs={12}>
            <div class="hidden-md-up" style={{ marginTop: -50 }} />
            <Paper style={styles.paper}>
              <CardContent>
                <div class="text-center">
                  <img
                    src="https://storage.googleapis.com/lastcampus/default_images/LastCampusLogo.png"
                    style={{ width: "120px" }}
                  />
                </div>
                <Typography style={styles.title} color="textSecondary" align="center" gutterBottom>
                  Sign in
                </Typography>
                <Typography
                  style={{ color: "#000000", fontSize: 14, fontWeight: 500 }}
                  color="textSecondary"
                  align="center"
                  gutterBottom
                >
                  to continue to LastCampus
                </Typography>
              </CardContent>

              <form role="form" autoComplete="off" onSubmit={this.onSubmit}>
                <div className="text-center">
                  <TextField
                    id="outlined-name"
                    name="email"
                    type="email"
                    value={this.state.email}
                    onChange={this.onChange}
                    error={errors.email}
                    label="Email"
                    style={styles.textField}
                    margin="normal"
                    variant="outlined"
                    autoComplete="off"
                  />
                  <TextFieldGroup
                    placeholder="Email Addresses"
                    name="email"
                    type="hidden"
                    value={this.state.email}
                    onChange={this.onChange}
                    error={errors.email}
                    autoComplete="off"
                  />
                  <TextField
                    id="outlined-name"
                    placeholder="Password"
                    name="password"
                    type="password"
                    label="Password"
                    value={this.state.password}
                    onChange={this.onChange}
                    error={errors.password}
                    style={styles.textField}
                    margin="normal"
                    variant="outlined"
                  />
                  <TextFieldGroup
                    placeholder="Password"
                    name="password"
                    type="hidden"
                    value={this.state.password}
                    onChange={this.onChange}
                    error={errors.password}
                  />

                  <div className="mb-3 m-1">
                    <Button variant="contained" color="primary" type="submit" value="submit" style={styles.buttonLogin}>
                      Login
                    </Button>
                    <Button variant="contained" component={Link} to={"/register"} style={styles.button}>
                      SignUp
                    </Button>
                  </div>
                  <Typography
                    style={{ color: "#424242", fontSize: 14, fontWeight: 500, paddingBottom: 30 }}
                    color="textSecondary"
                    align="center"
                    gutterBottom
                    component={Link}
                    to={"/contact"}
                  >
                    Forgot password? click here
                  </Typography>
                </div>
              </form>

              <SocialButton
                provider="facebook"
                appId="2303179986605135"
                onLoginSuccess={this.handleSocialLogin}
                onLoginFailure={this.handleSocialLoginFailure}
                scope="email"
              >
                Login with Facebook
              </SocialButton>
            </Paper>
          </Grid>
          <Grid item lg={4} sm={12} xs={12} />
        </Grid>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: propTypes.func.isRequired,
  auth: propTypes.object.isRequired,
  errors: propTypes.object.isRequired
};

const mapStatetoProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStatetoProps,
  { loginUser, loginUserFacebook }
)(Login);

```


### A trigger based cloud function which update user points when he perform any activity on the app

```
const functions = require("firebase-functions");
const admin = require("firebase-admin");

var serviceAccount = require("xxxx");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://saath-health.firebaseio.com"
});

var updateUserCategoryRef = require("./updateUserCategory");
var updateDocRef = require("./updateExistingDoc");
var addNewEntryRef = require("./addNewLeaderBoardEntry");
var updateTandCRef = require("./updateTandC");
var updateGenderRef = require("./updateGender");
var updateNoOfChildRef = require("./updateNoOfChilds");
var ageUpdateRef = require("./ageUpdate");

const VIDEO_POINT_ON_FIRST_WATCH = 100;
const VIDEO_POINT_ON_SECOND_WATCH = 50;
const QUIZ_POINTS_ON_CORRECT = 100;
const QUIZ_POINTS_ON_INCORRECT = 20;

const T_and_C_quiz_id = "terms_and_conditions_quiz";
const gender_update_quiz_id = "gender_update";
const no_of_child_update_quiz_id = "r11DEC19";
const age_update_quiz_id = "r11DEC19b";

exports.updateLeaderBoard = functions.firestore.document("userActivities/{docId}").onCreate((snap, context) => {
  // testing function
  const activityData = snap.data();
  const increment = admin.firestore.FieldValue.increment(1);
  const uid = activityData.user.id;
  const activity_type = activityData.aTyp;

  return new Promise(resolve => {
    admin
      .firestore()
      .collection("leaderBoard")
      .doc(uid)
      .get()
      .then(async doc => {
        if (doc.data() !== undefined) {
          switch (activity_type) {
            case "video":
              if (activityData.vdo.isCmp) {
                const pointsIncrement = admin.firestore.FieldValue.increment(VIDEO_POINT_ON_FIRST_WATCH);

                await updateDocRef("video", pointsIncrement, increment, doc.ref);
                await updateUserCategoryRef(doc.data(), "video");
                resolve("done 1");
              }
              break;
            case "quiz":
              {
                const pointsIncrement = admin.firestore.FieldValue.increment(
                  activityData.quiz.nCrt * QUIZ_POINTS_ON_CORRECT - activityData.quiz.nIcr * QUIZ_POINTS_ON_INCORRECT
                );
                const check =
                  activityData.quiz.nCrt * QUIZ_POINTS_ON_CORRECT - activityData.quiz.nIcr * QUIZ_POINTS_ON_INCORRECT;

                // check quiz id for any special quiz for updating profile
                const quiz_id = activityData.quiz.id;
                //check and update T&C status
                if (quiz_id === T_and_C_quiz_id) {
                  await updateTandCRef(uid, quiz_id);
                }

                //check update gender
                if (quiz_id === gender_update_quiz_id) {
                  await updateGenderRef(uid, quiz_id);
                }

                //check no of childrens update
                if (quiz_id === no_of_child_update_quiz_id) {
                  await updateNoOfChildRef(uid, quiz_id);
                }

                //check for age update quiz id
                if (quiz_id === age_update_quiz_id) {
                  await ageUpdateRef(uid, quiz_id);
                }

                if (doc.data().life.p <= 0 && check < 0) {
                  await updateDocRef("quiz", 0, increment, doc.ref);
                } else {
                  if (doc.data().life.p + check < 0) {
                    await updateDocRef("quiz", 0, increment, doc.ref);
                  } else {
                    await updateDocRef("quiz", pointsIncrement, increment, doc.ref);
                  }
                }
                await updateUserCategoryRef(doc.data(), "quiz");
                resolve("Done 2");
              }
              break;
            case "offer":
              if (activityData.ofr.isCf) {
                await updateDocRef("offer", null, increment, doc.ref);
                await updateUserCategoryRef(doc.data(), "offer");
                resolve("Done 3");
              }
              break;
          }

          resolve({ success: true, message: "Leaderboard updated" });
        } else {
          await addNewEntryRef(activityData);
          resolve("Done");
        }
      })
      .catch(err => {
        console.log(err);
        resolve("Error 22");
      });
  });
});

```
